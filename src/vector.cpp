#include "vector.hpp"

// region Constructors

template<class primitiveType, PrimitiveType enumType>
PrimitiveType _TypedVector<primitiveType, enumType>::getType() const
{ return eType; }

template<class baseType>
template<class primitiveType>
_DataVector<baseType>::_DataVector(const std::vector<primitiveType> &init, bool assign)
        : data(init.begin(), init.end()), assignable(assign)
{}

template<class baseType>
template<class primitiveType>
_DataVector<baseType>::_DataVector(primitiveType init)
        : data{std::move(init)}, assignable(true)
{}

template<class baseType>
_IndexedVector<baseType>::_IndexedVector(IVector src, std::vector<std::size_t> ind)
        : source(SP<baseType>(*src)), indices(std::move(ind))
{
    std::size_t len = source->getLength();
    auto max_it = std::max_element(indices.begin(), indices.end());
    std::size_t max_ind = max_it != indices.end() ? *max_it : 0;
    if (max_ind > len)
    { len = (source = source->expand(max_ind))->getLength(); }
    for (auto it = indices.begin(); it != indices.end();)
    {
        if (*it > len || !*it)
        { it = indices.erase(it); }
        if (it != indices.end())
        { ++it; }
    }
}

// endregion

// region Methods

template<class primitiveType, PrimitiveType enumType>
IVector _TypedVector<primitiveType, enumType>::index(IVector self, std::vector<std::size_t> indices) const
{ return createVector<IndexedVector<vType>>(self, indices); }

template<class primitiveType, PrimitiveType enumType>
TypedVector<class _TypedVector<primitiveType, enumType>::pType, _TypedVector<primitiveType, enumType>::eType>
_TypedVector<primitiveType, enumType>::assign(
        const _TypedVector<_TypedVector<primitiveType, enumType>::pType,
                _TypedVector<primitiveType, enumType>::eType> &rhs) const
{
    if (!isAssignable())
    { except(fmt("Attempting to assign to an unassignable entity (possibly a double-indexed vector)")); }
    std::vector<std::size_t> uind = unindex();
    std::vector<pType> data = getData();
    std::vector<pType> rdata = rhs.getValue();
    for (std::size_t index = 1; index <= uind.size(); ++index)
    { data[uind[index - 1] - 1] = rdata[index - 1]; }
    return createVector<DataVector<vType>>(data);
}

template<class baseType>
std::size_t _DataVector<baseType>::getLength() const
{ return data.size(); }

template<class baseType>
std::size_t _IndexedVector<baseType>::getLength() const
{ return indices.size(); }

template<class baseType>
std::vector<class baseType::pType> _DataVector<baseType>::getValue() const
{ return data; }

template<class baseType>
std::vector<class baseType::pType> _IndexedVector<baseType>::getValue() const
{
    std::size_t max_sz = getLength();
    std::vector<class baseType::pType> indexed;
    std::vector<class baseType::pType> data(source->getValue());
    indexed.reserve(max_sz);
    for (std::size_t index = 0; index < max_sz; ++index)
    { indexed.emplace_back(data[indices[index] - 1]); }

    return indexed;
}

template<class baseType>
class baseType::pType _DataVector<baseType>::at(std::size_t pos) const
{
    if (pos < data.size())
    { return data[pos]; }
    else
    {
        class baseType::pType pt;
        return pt;
    }
}

template<class baseType>
class baseType::pType _IndexedVector<baseType>::at(std::size_t pos) const
{
    if (pos < indices.size())
    { return source->at(indices[pos]); }
    else
    {
        class baseType::pType pt;
        return pt;
    }
}

template<class baseType>
bool _DataVector<baseType>::isAssignable() const
{ return assignable; }

template<class baseType>
bool _IndexedVector<baseType>::isAssignable() const
{ return source->isAssignable(); }

template<class baseType>
TypedVector<class baseType::pType, baseType::eType> _DataVector<baseType>::expand(std::size_t sz) const
{
    std::vector copy(data);
    copy.resize(sz);
    return createVector<DataVector<class baseType::vType>>(copy);
}

template<class baseType>
TypedVector<class baseType::pType, baseType::eType> _IndexedVector<baseType>::expand(std::size_t sz) const
{
    if (sz > getLength())
    { return createVector<DataVector<class baseType::vType>>(getValue(), false)->expand(sz); }
    else
    { return createVector<IndexedVector<class baseType::vType>>(source, indices); }
}

template<class baseType>
std::vector<std::size_t> _DataVector<baseType>::unindex() const
{
    std::vector<std::size_t> ind;
    for (std::size_t index = 1; index <= data.size(); ++index)
    { ind.emplace_back(index); }
    return ind;
}

template<class baseType>
std::vector<std::size_t> _IndexedVector<baseType>::unindex() const
{
    std::vector<std::size_t> ind = source->unindex();
    std::vector<std::size_t> uind;
    for (std::size_t index : indices)
    { uind.emplace_back(ind[index - 1]); }
    return uind;
}

template<class baseType>
std::vector<class baseType::pType> _DataVector<baseType>::getData() const
{ return data; }

template<class baseType>
std::vector<class baseType::pType> _IndexedVector<baseType>::getData() const
{ return source->getData(); }
// endregion

// region _DataVector type casting

template<class vectorType>
_DataVector<vectorType>::operator NullVector() const
{ return createVector<DataVector<NullVector>>(data); }

template<class vectorType>
_DataVector<vectorType>::operator CharacterVector() const
{ return createVector<DataVector<CharacterVector>>(data); }

template<class vectorType>
_DataVector<vectorType>::operator NumericVector() const
{ return createVector<DataVector<NumericVector>>(data); }

template<class vectorType>
_DataVector<vectorType>::operator LogicalVector() const
{ return createVector<DataVector<LogicalVector>>(data); }

template<class vectorType>
_DataVector<vectorType>::operator FunctionVector() const
{ return createVector<DataVector<FunctionVector>>(data); }
// endregion

// region _IndexedVector type casting

template<class vectorType>
_IndexedVector<vectorType>::operator NullVector() const
{ return createVector<IndexedVector<NullVector>>(NullVector(*source), indices); }

template<class vectorType>
_IndexedVector<vectorType>::operator CharacterVector() const
{ return createVector<IndexedVector<CharacterVector>>(CharacterVector(*source), indices); }

template<class vectorType>
_IndexedVector<vectorType>::operator NumericVector() const
{ return createVector<IndexedVector<NumericVector>>(NumericVector(*source), indices); }

template<class vectorType>
_IndexedVector<vectorType>::operator LogicalVector() const
{ return createVector<IndexedVector<LogicalVector>>(LogicalVector(*source), indices); }

template<class vectorType>
_IndexedVector<vectorType>::operator FunctionVector() const
{ return createVector<IndexedVector<FunctionVector>>(FunctionVector(*source), indices); }
// endregion

// region Vector operators

#define BINARY_OPERATOR(resultType, argumentType, boperator)                        \
resultType boperator(const _IVector &l, const _IVector &r)                          \
{                                                                                   \
    try                                                                             \
    {                                                                               \
        std::size_t max_sz = std::max(l.getLength(), r.getLength());                \
        std::vector lvalue = argumentType(l)->expand(max_sz)->getValue();           \
        std::vector rvalue = argumentType(r)->expand(max_sz)->getValue();           \
        std::vector<resultType::element_type::pType> result;                        \
        result.reserve(max_sz);                                                     \
        for (std::size_t index = 0; index < max_sz; ++index)                        \
        { result.emplace_back(boperator(lvalue[index], rvalue[index])); }           \
        return createVector<DataVector<resultType>>(result);                        \
    }                                                                               \
    catch (std::runtime_error &rte)                                                 \
    { except(rte.what() + std::string(" in "#boperator)); }                         \
}                                                                                   \

#define COMPARATOR_OPERATOR(boperator)                                              \
LogicalVector boperator(const _IVector &l, const _IVector &r)                       \
{                                                                                   \
    try                                                                             \
    {                                                                               \
        std::size_t max_sz = std::max(l.getLength(), r.getLength());                \
        std::vector<LogicalPrimitive> result;                                       \
        result.reserve(max_sz);                                                     \
        if (l.getType() == PrimitiveType::CHARACTER_PRIMITIVE ||                    \
            r.getType() == PrimitiveType::CHARACTER_PRIMITIVE)                      \
        {                                                                           \
            std::vector lvalue = CharacterVector(l)->expand(max_sz)->getValue();    \
            std::vector rvalue = CharacterVector(r)->expand(max_sz)->getValue();    \
            for (std::size_t index = 0; index < max_sz; ++index)                    \
            { result.emplace_back(boperator(lvalue[index], rvalue[index])); }       \
        }                                                                           \
        else                                                                        \
        {                                                                           \
            std::vector lvalue = NumericVector(l)->expand(max_sz)->getValue();      \
            std::vector rvalue = NumericVector(r)->expand(max_sz)->getValue();      \
            for (std::size_t index = 0; index < max_sz; ++index)                    \
            { result.emplace_back(boperator(lvalue[index], rvalue[index])); }       \
        }                                                                           \
        return createVector<DataVector<LogicalVector>>(result);                     \
    }                                                                               \
    catch (std::runtime_error & rte)                                                \
    { except(rte.what() + std::string(" in "#boperator)); }                         \
}                                                                                   \

#define UNARY_OPERATOR(resultType, argumentType, uoperator)                         \
resultType uoperator(const _IVector &v)                                             \
{                                                                                   \
    try                                                                             \
    {                                                                               \
        std::size_t max_sz = v.getLength();                                         \
        std::vector value = argumentType(v)->getValue();                            \
        std::vector<resultType::element_type::pType> result;                        \
        result.reserve(max_sz);                                                     \
        for (std::size_t index = 0; index < max_sz; ++index)                        \
        { result.emplace_back(uoperator(value[index])); }                           \
        return createVector<DataVector<resultType>>(result);                        \
    }                                                                               \
    catch (std::runtime_error &rte)                                                 \
    { except(rte.what() + std::string(" in "#uoperator)); }                         \
}                                                                                   \


BINARY_OPERATOR(NumericVector, NumericVector, operator+);

BINARY_OPERATOR(NumericVector, NumericVector, operator-);

BINARY_OPERATOR(NumericVector, NumericVector, operator*);

BINARY_OPERATOR(NumericVector, NumericVector, operator/);

BINARY_OPERATOR(LogicalVector, LogicalVector, operator&);

BINARY_OPERATOR(LogicalVector, LogicalVector, operator|);

COMPARATOR_OPERATOR(operator<);

COMPARATOR_OPERATOR(operator>);

COMPARATOR_OPERATOR(operator<=);

COMPARATOR_OPERATOR(operator>=);

COMPARATOR_OPERATOR(operator==);

COMPARATOR_OPERATOR(operator!=);

UNARY_OPERATOR(NumericVector, NumericVector, operator+);

UNARY_OPERATOR(NumericVector, NumericVector, operator-);

UNARY_OPERATOR(LogicalVector, LogicalVector, operator!);

std::ostream &operator<<(std::ostream &os, const _IVector &v)
{
    if (!v.getLength())
    { os << *v.getType() << "(0)"; }
    else if (v.getType() == PrimitiveType::CHARACTER_PRIMITIVE)
    {
        std::vector<CharacterPrimitive> cv = CharacterVector(v)->getValue();
        for (std::size_t index = 0; index < cv.size(); ++index)
        { os << (cv[index].isNull ? "NULL" : "\"" + cv[index].value + "\"") << ((index + 1) % 8 ? " " : "\n"); }
    }
    else
    {
        std::vector<CharacterPrimitive> cv = CharacterVector(v)->getValue();
        for (std::size_t index = 0; index < cv.size(); ++index)
        { os << (cv[index].isNull ? "NULL" : cv[index].value) << ((index + 1) % 8 ? " " : "\n"); }
    }
    return os;
}

IVector operator<<=(const _IVector &lhs, const _IVector &rhs)
{
    std::size_t lhs_len = lhs.getLength();
    if (lhs.getType() == PrimitiveType::CHARACTER_PRIMITIVE || rhs.getType() == PrimitiveType::CHARACTER_PRIMITIVE)
    { return CharacterVector(lhs)->assign(*CharacterVector(rhs)->expand(lhs_len)); }
    else if (lhs.getType() == PrimitiveType::NUMERIC_PRIMITIVE || rhs.getType() == PrimitiveType::NUMERIC_PRIMITIVE)
    { return NumericVector(lhs)->assign(*NumericVector(rhs)->expand(lhs_len)); }
    else if (lhs.getType() == PrimitiveType::LOGICAL_PRIMITIVE || rhs.getType() == PrimitiveType::LOGICAL_PRIMITIVE)
    { return LogicalVector(lhs)->assign(*LogicalVector(rhs)->expand(lhs_len)); }
    else if (lhs.getType() == PrimitiveType::FUNCTION_PRIMITIVE || rhs.getType() == PrimitiveType::FUNCTION_PRIMITIVE)
    { return FunctionVector(lhs)->assign(*FunctionVector(rhs)->expand(lhs_len)); }
    else
    { return NullVector(lhs)->assign(*NullVector(rhs)->expand(lhs_len)); }
}
// endregion

// region Explicit template instantiation

template _DataVector<_NullVector>::_DataVector(const std::vector<NullPrimitive> &, bool);
template _DataVector<_NumericVector>::_DataVector(const std::vector<NumericPrimitive> &, bool);
template _DataVector<_CharacterVector>::_DataVector(const std::vector<CharacterPrimitive> &, bool);
template _DataVector<_LogicalVector>::_DataVector(const std::vector<LogicalPrimitive> &, bool);
template _DataVector<_FunctionVector>::_DataVector(const std::vector<FunctionPrimitive> &, bool);

template _DataVector<_NullVector>::_DataVector(NullPrimitive);
template _DataVector<_NumericVector>::_DataVector(NumericPrimitive);
template _DataVector<_CharacterVector>::_DataVector(CharacterPrimitive);
template _DataVector<_LogicalVector>::_DataVector(LogicalPrimitive);
template _DataVector<_FunctionVector>::_DataVector(FunctionPrimitive);

template class _TypedVector<NullPrimitive, PrimitiveType::NULL_PRIMITIVE>;
template class _DataVector<_NullVector>;
template class _IndexedVector<_NullVector>;

template class _TypedVector<NumericPrimitive, PrimitiveType::NUMERIC_PRIMITIVE>;
template class _DataVector<_NumericVector>;
template class _IndexedVector<_NumericVector>;

template class _TypedVector<CharacterPrimitive, PrimitiveType::CHARACTER_PRIMITIVE>;
template class _DataVector<_CharacterVector>;
template class _IndexedVector<_CharacterVector>;

template class _TypedVector<LogicalPrimitive, PrimitiveType::LOGICAL_PRIMITIVE>;
template class _DataVector<_LogicalVector>;
template class _IndexedVector<_LogicalVector>;

template class _TypedVector<FunctionPrimitive, PrimitiveType::FUNCTION_PRIMITIVE>;
template class _DataVector<_FunctionVector>;
template class _IndexedVector<_FunctionVector>;
// endregion