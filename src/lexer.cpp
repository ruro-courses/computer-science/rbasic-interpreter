#include <sstream>
#include <iostream>

#include "lexer.hpp"

using LexemeType = Lexeme::LexemeType;

[[noreturn]] void exceptInvalidSymbol(char symbol)
{
    except(fmt("Invalid symbol ",
               std::isgraph(symbol) ?
               fmt('\'', symbol, '\'') :
               fmt("0x", std::hex, static_cast<int>(symbol))));
}

bool isValidInIdentifier(char symbol, bool beginning)
{ return std::isalpha(symbol) || symbol == '.' || symbol == '_' || (!beginning && std::isdigit(symbol)); };

std::string::size_type Lexer::isPrefixedWith(const std::string &prefix)
{
    std::string::size_type len = prefix.length();
    if (currentLine().length() - currentSymbolNum >= len &&
        std::equal(prefix.begin(), prefix.end(), currentLine().begin() + currentSymbolNum) &&
        !isValidInIdentifier(currentSymbol(len), false))
    { return len; }
    else
    { return 0; }
};

void Lexer::skipWhiteSpace()
{
    std::string::size_type len = 0;
    while (currentSymbol(len) && currentSymbol(len) != '\n' && std::isspace(currentSymbol(len)))
    { len++; }
    currentSymbolNum += len;
};

Lexeme Lexer::extractLexeme(LexemeType lt, std::string::size_type len)
{
    len = std::min(len, currentLine().length() - currentSymbolNum);
    std::string_view lc = std::string_view(currentLine()).substr(currentSymbolNum, len);
    currentSymbolNum += len;
    skipWhiteSpace();
    return Lexeme(lt, lc);
};

Lexeme Lexer::extractCharacter()
{
    std::string::size_type len = 1;
    while (currentSymbol(len) && currentSymbol(len) != '"')
    { len += 1 + (currentSymbol(len) == '\\' && currentSymbol(len + 1)); }
    if (currentSymbol(len) != '"')
    { except("Unterminated Character literal"); }
    return extractLexeme(LexemeType::LEX_CHARACTER, len + 1);
}

Lexeme Lexer::extractNumeric()
{
    bool dot = false;
    bool lastDot = false;
    std::string::size_type len = 0;
    while (currentSymbol(len))
    {
        if (std::isdigit(currentSymbol(len)))
        { lastDot = false; }
        else if (!dot && currentSymbol(len) == '.')
        { dot = lastDot = true; }
        else if (!isValidInIdentifier(currentSymbol(len), false) && !lastDot)
        { break; }
        else
        { exceptInvalidSymbol(currentSymbol(len)); }
        len++;
    }
    return extractLexeme(LexemeType::LEX_NUMERIC, len);
};

Lexeme Lexer::extractIdentifier()
{
    std::string::size_type len = 1;
    while (isValidInIdentifier(currentSymbol(len), false))
    { len++; }
    return extractLexeme(LexemeType::LEX_IDENTIFIER, len);
}

Lexeme Lexer::extractComment()
{
    std::string::size_type len = 1;
    while (currentSymbol(len) && currentSymbol(len) != '\n')
    { len++; }
    currentSymbolNum += len;
    return extractLexeme(LexemeType::LEX_LINE, 1);
}

Lexeme Lexer::nextLexem()
{
    skipWhiteSpace();
    switch (currentSymbol())
    {
        case '\n':return extractLexeme(LexemeType::LEX_LINE, 1);
        case ';': return extractLexeme(LexemeType::LEX_SEMICOLON, 1);
        case ',': return extractLexeme(LexemeType::LEX_ARGUMENT_COMMA, 1);
        case ':': return extractLexeme(LexemeType::LEX_OP_RANGE, 1);
        case '"': return extractCharacter();
        case '#': return extractComment();
        case '(': [[fallthrough]]
        case ')': return extractLexeme(LexemeType::LEX_PAREN, 1);
        case '[': [[fallthrough]]
        case ']': return extractLexeme(LexemeType::LEX_INDEX, 1);
        case '{': [[fallthrough]]
        case '}': return extractLexeme(LexemeType::LEX_BLOCK, 1);
        case '+': [[fallthrough]]
        case '-': return extractLexeme(LexemeType::LEX_OP_SUM, 1);
        case '*': [[fallthrough]]
        case '/': return extractLexeme(LexemeType::LEX_OP_PRODUCT, 1);
        case '&': [[fallthrough]]
        case '|': return extractLexeme(LexemeType::LEX_OP_LOGIC, 1);

        case '!':
            if (currentSymbol(1) == '=')
            { return extractLexeme(LexemeType::LEX_OP_COMPARE, 2); }
            else
            { return extractLexeme(LexemeType::LEX_OP_LOGIC, 1); }

        case '=':
            if (currentSymbol(1) == '=')
            { return extractLexeme(LexemeType::LEX_OP_COMPARE, 2); }
            else
            { return extractLexeme(LexemeType::LEX_ARGUMENT_ASSIGN, 1); }

        case '<':
            if (currentSymbol(1) == '-')
            { return extractLexeme(LexemeType::LEX_OP_ASSIGN, 2); }
            else if (currentSymbol(1) == '=')
            { return extractLexeme(LexemeType::LEX_OP_COMPARE, 2); }
            else
            { return extractLexeme(LexemeType::LEX_OP_COMPARE, 1); }

        case '>':
            if (currentSymbol(1) == '=')
            { return extractLexeme(LexemeType::LEX_OP_COMPARE, 2); }
            else
            { return extractLexeme(LexemeType::LEX_OP_COMPARE, 1); }

        default:
        {
            std::string::size_type len;
            if ((len = isPrefixedWith("function")) ||
                (len = isPrefixedWith("while")))
            { return extractLexeme(LexemeType::LEX_KEYWORD, len); }
            else if ((len = isPrefixedWith("NULL")) ||
                     (len = isPrefixedWith("TRUE")) ||
                     (len = isPrefixedWith("FALSE")))
            { return extractLexeme(LexemeType::LEX_CONSTANT, len); }
            else if (std::isdigit(currentSymbol()) || (currentSymbol() == '.' && std::isdigit(currentSymbol(1))))
            { return extractNumeric(); }
            else if (isValidInIdentifier(currentSymbol(), true))
            { return extractIdentifier(); }
            else
            { exceptInvalidSymbol(currentSymbol()); }
        }
    }
}

Lexer::Lexer(std::function<std::string(bool)> src)
        : source(std::move(src)), lines(1), currentLineNum(), currentSymbolNum()
{}

Lexeme Lexer::operator()(bool continuation)
{
    while (!(currentLine().length() - currentSymbolNum))
    {
        currentLineNum++;
        currentSymbolNum = 0;
        readMore(continuation);
    }
    return nextLexem();
}

void Lexer::readMore(bool continuation)
{
    while (currentLineNum >= lines.size())
    {
        std::istringstream src(source(continuation));
        std::string temp;
        while (std::getline(src, temp))
        { lines.push_back(temp + "\n"); }
    }
}

std::string &Lexer::currentLine(std::vector<std::string>::size_type offset)
{
    return lines[currentLineNum + offset];
}

char Lexer::currentSymbol(std::vector<std::string>::size_type offset)
{ return currentLine()[currentSymbolNum + offset]; }
