#include "builtins.hpp"

IVector concatenateBuiltin(std::vector<IVector> posArgs, std::vector<std::pair<std::string, IVector>> namedArgs)
{
    if (!namedArgs.empty())
    { except(fmt("Concatenation builtin doesn't accept named arguments")); }
    if (posArgs.empty())
    { return createVector<DataVector<NullVector >>(std::vector<NullPrimitive>()); }
    IVector iv = posArgs[0];
    for (std::size_t index = 1; index < posArgs.size(); ++index)
    {
        std::vector<std::size_t> indices(posArgs[index]->getLength());
        std::iota(indices.begin(), indices.end(), iv->getLength() + 1);
        iv = *iv->index(iv, indices) <<= *posArgs[index];
    }
    return iv;
}

IVector modeBuiltin(std::vector<IVector> posArgs, std::vector<std::pair<std::string, IVector>> namedArgs)
{
    if (!namedArgs.empty())
    { except(fmt("Mode builtin doesn't accept named arguments")); }
    if (posArgs.size() != 1)
    { except(fmt("Mode builtin needs 1 positional argument, got ", posArgs.size())); }
    return createVector<DataVector<CharacterVector>>(
            CharacterPrimitive(*posArgs[0]->getType())
    );
}

IVector lengthBuiltin(std::vector<IVector> posArgs, std::vector<std::pair<std::string, IVector>> namedArgs)
{
    if (!namedArgs.empty())
    { except(fmt("Length builtin doesn't accept named arguments")); }
    if (posArgs.size() != 1)
    { except(fmt("Length builtin needs 1 positional argument, got ", posArgs.size())); }
    return createVector<DataVector<NumericVector>>(
            NumericPrimitive(static_cast<double>(posArgs[0]->getLength()))
    );
}