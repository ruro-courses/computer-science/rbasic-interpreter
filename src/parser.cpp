#include "parser.hpp"

using LexemeType = Lexeme::LexemeType;

Parser::Parser(std::function<Lexeme(bool)> src)
        : source(std::move(src)), lexems(), currentPosition(), expressionDepth()
{}

const Lexeme &Parser::at(std::vector<Lexeme>::size_type pos, bool force_continuation)
{
    bool continuation = force_continuation || expressionDepth;
    while (currentPosition + pos >= lexems.size())
    { lexems.push_back(source(continuation)); }
    while (continuation && lexems[currentPosition + pos].getType() == LexemeType::LEX_LINE)
    {
        lexems.push_back(source(continuation));
        currentPosition++;
    }
    return lexems[currentPosition + pos];
}

IExpression getNumeric(std::string_view num)
{
    IVector c;
    std::size_t len = 0;
    c = createVector<DataVector<NumericVector>>(NumericPrimitive(std::stod(std::string(num), &len)));
    if (len != num.size())
    { except(fmt("Numeric constant ", num, " is not valid")); }
    return createExpression<ConstantExpression>(c);
}

IExpression getCharacter(std::string_view cha)
{
    IVector c;
    std::ostringstream oss;
    auto it = cha.begin();
    if (*it++ != '\"')
    { except(fmt("Missing quote in character Literal")); }
    for (; it != cha.end();)
    {
        if (*it == '\\')
        {
            ++it;
            switch (*it)
            {
                case '\\': [[fallthrough]]
                case '\'': [[fallthrough]]
                case '\"':
                {
                    oss << *it;
                    break;
                }
                case 'n':
                {
                    oss << std::endl;
                    break;
                }
                default:except(fmt("Unsupported escape symbol \\", *it));
            }
        }
        else if (it + 1 == cha.end())
        {
            if (*it++ != '\"')
            { except(fmt("Missing quote in character Literal")); }
            else
            { break; }
        }
        else
        { oss << *it; }
        if (it != cha.end())
        { ++it; }
    }
    c = createVector<DataVector<CharacterVector>>(CharacterPrimitive(oss.str()));
    return createExpression<ConstantExpression>(c);
}

IExpression getConstant(std::string_view con)
{
    IVector c;
    if (con == "NULL")
    { c = createVector<DataVector<NullVector>>(NullPrimitive()); }
    else if (con == "TRUE")
    { c = createVector<DataVector<LogicalVector>>(LogicalPrimitive(true)); }
    else if (con == "FALSE")
    { c = createVector<DataVector<LogicalVector>>(LogicalPrimitive(false)); }
    else
    { except(fmt("Unknown constant type ", con)); }
    return createExpression<ConstantExpression>(c);
}

IExpression getIdentifier(std::string_view ident)
{
    return createExpression<IdentifierExpression>(std::string(ident));
}

IExpression Parser::getNamed()
{
    Lexeme lex = at(0, true);
    currentPosition++;
    switch (lex.getType())
    {
        case LexemeType::LEX_IDENTIFIER: return getIdentifier(lex.getContents());
        case LexemeType::LEX_CONSTANT: return getConstant(lex.getContents());
        case LexemeType::LEX_NUMERIC: return getNumeric(lex.getContents());
        case LexemeType::LEX_CHARACTER: return getCharacter(lex.getContents());
        default:
        {
            currentPosition--;
            return createExpression<NullExpression>();
        }
    }
}

IExpression Parser::getSpecial()
{
    Lexeme lex = at();
    if (lex.getType() == LexemeType::LEX_KEYWORD)
    {
        if (lex.getContents() == "while")
        {
            currentPosition++;
            IExpression condition = getParenGroup();
            IExpression body = getExpression();
            return createExpression<WhileExpression>(condition, body);
        }
        else if (lex.getContents() == "function")
        {
            std::vector<std::pair<std::string, IExpression>> argData;
            currentPosition++;
            lex = at();
            if (lex.getType() != LexemeType::LEX_PAREN || lex.getContents() != "(")
            { except(fmt("Unbalanced parentheses")); }
            expressionDepth++;
            while (lex.getType() != LexemeType::LEX_PAREN || lex.getContents() != ")")
            {
                currentPosition++;
                lex = at();
                if (lex.getType() != LexemeType::LEX_IDENTIFIER)
                { except(fmt("Expected identifier in function specification, got:",
                             *lex.getType(), "(", lex.getContents(), ")")); }
                std::string name = lex.getContents();
                currentPosition++;
                lex = at();
                IExpression def;
                if (lex.getType() == LexemeType::LEX_ARGUMENT_COMMA ||
                        (lex.getType() == LexemeType::LEX_PAREN && lex.getContents() == ")"))
                { def = createExpression<NullExpression>(); }
                else if (lex.getType() == LexemeType::LEX_ARGUMENT_ASSIGN)
                {
                    def = getExpression();
                    lex = at();
                    if (lex.getType() != LexemeType::LEX_ARGUMENT_COMMA &&
                            (lex.getType() != LexemeType::LEX_PAREN || lex.getContents() != ")"))
                    { except(fmt("Expected comma or function specification end, got:",
                                 *lex.getType(), "(", lex.getContents(), ")")); }
                }
                else
                { except(fmt("Expected comma, argument default assignment or function specification end, got:",
                             *lex.getType(), "(", lex.getContents(), ")")); }
                argData.emplace_back(std::make_pair(name, def));
            }
            if (!expressionDepth--)
            { except(fmt("Unbalanced parentheses")); }
            currentPosition++;
            IExpression body = getExpression();
            return createExpression<FunctionExpression>(body, argData);
        }
        else if (lex.getContents() == "break")
        { return createExpression<BreakExpression>(); }
        else if (lex.getContents() == "if")
        {
            IExpression condition = getParenGroup();
            IExpression onTrue = getExpression();
            lex = at();
            IExpression onFalse =
                    (lex.getType() == LexemeType::LEX_KEYWORD && lex.getContents() == "else") ?
                    getExpression() : createExpression<NullExpression>();
            return createExpression<IfExpression>(condition, onTrue, onFalse);
        }
        else if (lex.getContents() == "else")
        { except("Unexpected else statement"); }
        else
        { except(fmt("Unknown keyword: \"", lex.getContents(), "\"")); }
    }
    else
    { return getNamed(); }
}

IExpression Parser::getParenGroup()
{
    Lexeme lex = at();
    if (lex.getType() != LexemeType::LEX_PAREN || lex.getContents() != "(")
    { except(fmt("Unbalanced parentheses")); }
    expressionDepth++;
    currentPosition++;
    IExpression value = getExpression();
    lex = at();
    if (lex.getType() != LexemeType::LEX_PAREN || lex.getContents() != ")")
    { except(fmt("Unexpected Lexeme: ", *lex.getType(), "(", lex.getContents(), ")")); }
    currentPosition++;
    if (!expressionDepth--)
    { except(fmt("Unbalanced parentheses")); }
    return value;
}

IExpression Parser::getSubExpression()
{
    Lexeme lex = at();
    auto getBlockGroup =
            [&]()
            {
                expressionDepth++;
                std::vector<IExpression> block;
                currentPosition++;
                do
                {
                    block.emplace_back(getExpression());
                    lex = at();
                } while (lex.getType() != LexemeType::LEX_BLOCK || lex.getContents() != "}");
                currentPosition++;
                if (!expressionDepth--)
                { except(fmt("Unbalanced parentheses in block expression")); }
                return block;
            };
    if (lex.getType() == LexemeType::LEX_PAREN && lex.getContents() == "(")
    { return getParenGroup(); }
    else if (lex.getType() == LexemeType::LEX_BLOCK && lex.getContents() == "{")
    { return createExpression<BlockExpression>(getBlockGroup()); }
    else
    { return getSpecial(); }
}

IExpression Parser::getElementary()
{
    IExpression val = getSubExpression();
    Lexeme lex = at();
    auto getCallArgs =
            [&]()
            {
                std::vector<std::pair<std::string, IExpression>> namedArgs;
                std::vector<IExpression> posArgs;
                currentPosition++;
                expressionDepth++;
                while (true)
                {
                    lex = at();
                    if (lex.getType() == LexemeType::LEX_IDENTIFIER &&
                        at(1).getType() == LexemeType::LEX_ARGUMENT_ASSIGN)
                    {
                        currentPosition += 2;
                        namedArgs.emplace_back(std::make_pair(lex.getContents(), getExpression()));
                    }
                    else
                    { posArgs.emplace_back(getExpression()); }
                    lex = at();
                    if (lex.getType() == LexemeType::LEX_ARGUMENT_COMMA)
                    { currentPosition++; }
                    else if (lex.getType() == LexemeType::LEX_PAREN && lex.getContents() == ")")
                    {
                        currentPosition++;
                        break;
                    }
                    else
                    {
                        except(fmt("Unexpected Lexeme in function arguments: ",
                                   *lex.getType(), "(", lex.getContents(), ")"));
                    }
                }
                if (!expressionDepth--)
                { except(fmt("Unbalanced parentheses in function call")); }
                return std::make_pair(posArgs, namedArgs);

            };
    auto getIndex =
            [&]()
            {
                currentPosition++;
                expressionDepth++;
                IExpression value = getExpression();
                lex = at();
                if (lex.getType() != LexemeType::LEX_INDEX || lex.getContents() != "]")
                {
                    except(fmt("Unbalanced brackets in index, get lexem ",
                               *lex.getType(), "(", lex.getContents(), ")"));
                }
                currentPosition++;
                if (!expressionDepth--)
                { except(fmt("Unbalanced parentheses in indexing expression")); }
                return value;
            };
    while ((lex.getType() == LexemeType::LEX_PAREN && lex.getContents() == "(") ||
           (lex.getType() == LexemeType::LEX_INDEX && lex.getContents() == "["))
    {
        if (lex.getType() == LexemeType::LEX_PAREN && lex.getContents() == "(")
        {
            auto[posArgs, namedArgs] = getCallArgs();
            val = createExpression<CallExpression>(val, posArgs, namedArgs);
        }
        else if (lex.getType() == LexemeType::LEX_INDEX && lex.getContents() == "[")
        { val = createExpression<IndexedExpression>(val, getIndex()); }
        lex = at();
    }
    return val;
}

IExpression Parser::getRange()
{
    Lexeme lex = at();
    std::vector<IExpression> range;
    do
    {
        range.emplace_back(getElementary());
        lex = at();
    } while (lex.getType() == LexemeType::LEX_OP_RANGE && ++currentPosition);
    return range.size() == 1 ? range[0] : createExpression<RangeExpression>(range);
}

IExpression Parser::getProduct()
{
    Lexeme lex = at();
    bool division = false;
    std::vector<std::pair<IExpression, bool>> prod;
    do
    {
        prod.emplace_back(std::make_pair(getRange(), division));
        lex = at();
        division = lex.getType() == LexemeType::LEX_OP_PRODUCT && lex.getContents() == "/";
    } while (lex.getType() == LexemeType::LEX_OP_PRODUCT && ++currentPosition);
    return prod.size() == 1 ? prod[0].first : createExpression<ProductExpression>(prod);
}

IExpression Parser::getSum()
{
    Lexeme lex = at();
    bool force;
    bool minus = false;
    if ((force = lex.getType() == LexemeType::LEX_OP_SUM) && ++currentPosition)
    { minus = lex.getContents() == "-"; }
    IExpression val = getProduct();
    lex = at();
    if (!force && lex.getType() != LexemeType::LEX_OP_SUM)
    { return val; }

    std::vector<std::pair<IExpression, bool>> sum{std::make_pair(val, minus)};
    while (lex.getType() == LexemeType::LEX_OP_SUM)
    {
        currentPosition++;
        minus = lex.getContents() == "-";
        val = getProduct();
        lex = at();
        sum.emplace_back(std::make_pair(val, minus));
    }
    return createExpression<SumExpression>(sum);
}

IExpression Parser::getComparative()
{
    using ComparatorType = _ComparativeExpression::ComparatorType;
    IExpression lhs = getSum();
    Lexeme lex = at();
    if (lex.getType() != LexemeType::LEX_OP_COMPARE)
    { return lhs; }

    currentPosition++;
    IExpression rhs = getSum();
    ComparatorType ct;
    if (lex.getContents() == "==")
    { ct = ComparatorType::EQ; }
    else if (lex.getContents() == "!=")
    { ct = ComparatorType::NEQ; }
    else if (lex.getContents() == "<")
    { ct = ComparatorType::LT; }
    else if (lex.getContents() == ">")
    { ct = ComparatorType::GT; }
    else if (lex.getContents() == "<=")
    { ct = ComparatorType::LTE; }
    else if (lex.getContents() == ">=")
    { ct = ComparatorType::GTE; }
    else
    { except(fmt("Invalid comparator type", lex.getContents())); }
    return createExpression<ComparativeExpression>(lhs, ct, rhs);
}

IExpression Parser::getLogical()
{
    Lexeme lex = at();
    IExpression val;
    auto getNot =
            [&]()
            {
                lex = at();
                std::size_t inverted = 0;
                while (lex.getType() == LexemeType::LEX_OP_LOGIC && lex.getContents() == "!")
                {
                    inverted++;
                    currentPosition++;
                    lex = at();
                }
                return inverted;
            };
    std::size_t count = 0;
    std::vector<std::pair<IExpression, bool>> and_val;
    std::vector<std::vector<std::pair<IExpression, bool>>> or_val;
    do
    {
        do
        {
            bool inverted = (count = getNot()) % 2 != 0;
            val = getComparative();
            and_val.emplace_back(std::make_pair(val, inverted));
            lex = at();
        } while (lex.getType() == LexemeType::LEX_OP_LOGIC && lex.getContents() == "&" && ++currentPosition);
        or_val.push_back(std::move(and_val));
        and_val.clear();
        lex = at();
    } while (lex.getType() == LexemeType::LEX_OP_LOGIC && ++currentPosition);
    if (or_val.size() == 1 && or_val[0].size() == 1 && !count)
    { return or_val[0][0].first; }
    return createExpression<LogicalExpression>(or_val);
}

IExpression Parser::getExpression()
{
    IExpression val = getLogical();
    Lexeme lex = at();
    if (lex.getType() != LexemeType::LEX_OP_ASSIGN)
    { return val; }

    std::vector<IExpression> sub;
    while (lex.getType() == LexemeType::LEX_OP_ASSIGN)
    {
        sub.push_back(val);
        currentPosition++;
        val = getLogical();
        lex = at();
    }
    return createExpression<AssignmentExpression>(sub, val);
}

IExpression Parser::operator()()
{
    IExpression ex = getExpression();
    Lexeme lex = at();
    currentPosition++;
    if (lex.getType() == LexemeType::LEX_LINE || lex.getType() == LexemeType::LEX_SEMICOLON)
    { return ex; }
    else
    { except(fmt("Expected expression to end, instead got ", *lex.getType(), "(", lex.getContents(), ")")); }
}
