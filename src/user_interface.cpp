#include "user_interface.hpp"

std::string getLineFromTerminal(bool continuation)
{
    std::cout << (continuation ? "+ " : "> ");
    std::string buffer;
    std::getline(std::cin, buffer);
    return buffer;
}

std::function<std::string(bool)> getGetterFromFile(std::string filename)
{
    auto getter =
            [filename](bool) mutable
            {
                static std::ifstream ifile(filename);
                std::string buffer;
                std::getline(ifile, buffer);
                return buffer;
            };
    return getter;
};

[[noreturn]] void except(const std::string &message)
{ throw std::runtime_error(message); }