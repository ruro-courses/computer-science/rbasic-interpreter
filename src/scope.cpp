#include "scope.hpp"

ScopeStack::ScopeStack(std::map<std::string, IVector> builtins) : ss{std::move(builtins)}
{ pushScope(); }

void ScopeStack::save(const std::string &name, IVector value)
{ ss.back().insert_or_assign(name, value); }

IVector ScopeStack::load(const std::string &name)
{
    for (auto scope = ss.rbegin(); scope != ss.rend(); ++scope)
    {
        if (scope->count(name))
        { return scope->at(name); }
    }
    except(fmt("Identifier ", name, " wasn't found in current scope"));
}

void ScopeStack::pushScope()
{ ss.emplace_back(); }

void ScopeStack::popScope()
{
    if (ss.size() > 2)
    { ss.pop_back(); }
    else
    { except(fmt("Attempting to exit from outermost scope")); }
}

bool ScopeStack::check(const std::string &name)
{
    for (auto scope = ss.rbegin(); scope != ss.rend(); ++scope)
    {
        if (scope->count(name))
        { return true; }
    }
    return false;
}
