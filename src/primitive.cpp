#include <cmath>
#include "primitive.hpp"

// region Constructors

NumericPrimitive::NumericPrimitive() : value(), isNull(true)
{}

CharacterPrimitive::CharacterPrimitive() : value(), isNull(true)
{}

LogicalPrimitive::LogicalPrimitive() : value(), isNull(true)
{}

FunctionPrimitive::FunctionPrimitive() : value(), isNull(true)
{}

NumericPrimitive::NumericPrimitive(double v, bool n) : value(v), isNull(n)
{}

CharacterPrimitive::CharacterPrimitive(std::string s, bool n) : value(std::move(s)), isNull(n)
{}

LogicalPrimitive::LogicalPrimitive(bool v, bool n) : value(v), isNull(n)
{}

FunctionPrimitive::FunctionPrimitive(ICallable v, bool n) : value(std::move(v)), isNull(n)
{}
// endregion

// region Short constructor templates

template<> NumericPrimitive::NumericPrimitive(double v)
        : NumericPrimitive(v, false)
{}

template<> CharacterPrimitive::CharacterPrimitive(std::string v)
        : CharacterPrimitive(std::move(v), false)
{}

template<> LogicalPrimitive::LogicalPrimitive(bool v)
        : LogicalPrimitive(v, false)
{}

template<> FunctionPrimitive::FunctionPrimitive(ICallable v)
        : FunctionPrimitive(std::move(v), false)
{}

// endregion

// region Numeric arithmetic

NumericPrimitive operator+(const NumericPrimitive &p)
{ return NumericPrimitive(+p.value, p.isNull); }

NumericPrimitive operator-(const NumericPrimitive &p)
{ return NumericPrimitive(-p.value, p.isNull); }

NumericPrimitive operator+(const NumericPrimitive &l, const NumericPrimitive &r)
{ return NumericPrimitive(l.value + r.value, l.isNull || r.isNull); }

NumericPrimitive operator-(const NumericPrimitive &l, const NumericPrimitive &r)
{ return NumericPrimitive(l.value - r.value, l.isNull || r.isNull); }

NumericPrimitive operator*(const NumericPrimitive &l, const NumericPrimitive &r)
{ return NumericPrimitive(l.value * r.value, l.isNull || r.isNull); }

NumericPrimitive operator/(const NumericPrimitive &l, const NumericPrimitive &r)
{ return NumericPrimitive(l.value / r.value, l.isNull || r.isNull); }
// endregion

// region Numeric comparisons

LogicalPrimitive operator<(const NumericPrimitive &l, const NumericPrimitive &r)
{ return LogicalPrimitive(l.value < r.value, l.isNull || r.isNull); }

LogicalPrimitive operator<=(const NumericPrimitive &l, const NumericPrimitive &r)
{ return LogicalPrimitive(l.value <= r.value, l.isNull || r.isNull); }

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"

LogicalPrimitive operator==(const NumericPrimitive &l, const NumericPrimitive &r)
{ return LogicalPrimitive(l.value == r.value, l.isNull || r.isNull); }

LogicalPrimitive operator!=(const NumericPrimitive &l, const NumericPrimitive &r)
{ return LogicalPrimitive(l.value != r.value, l.isNull || r.isNull); }

#pragma GCC diagnostic pop

LogicalPrimitive operator>(const NumericPrimitive &l, const NumericPrimitive &r)
{ return r < l; }

LogicalPrimitive operator>=(const NumericPrimitive &l, const NumericPrimitive &r)
{ return r <= l; }
// endregion

// region Character comparisons

LogicalPrimitive operator<(const CharacterPrimitive &l, const CharacterPrimitive &r)
{ return LogicalPrimitive(l.value < r.value, l.isNull || r.isNull); }

LogicalPrimitive operator<=(const CharacterPrimitive &l, const CharacterPrimitive &r)
{ return LogicalPrimitive(l.value <= r.value, l.isNull || r.isNull); }

LogicalPrimitive operator==(const CharacterPrimitive &l, const CharacterPrimitive &r)
{ return LogicalPrimitive(l.value == r.value, l.isNull || r.isNull); }

LogicalPrimitive operator!=(const CharacterPrimitive &l, const CharacterPrimitive &r)
{ return LogicalPrimitive(l.value != r.value, l.isNull || r.isNull); }

LogicalPrimitive operator>(const CharacterPrimitive &l, const CharacterPrimitive &r)
{ return r < l; }

LogicalPrimitive operator>=(const CharacterPrimitive &l, const CharacterPrimitive &r)
{ return r >= l; }
// endregion

// region Logical operations

LogicalPrimitive operator!(const LogicalPrimitive &p)
{ return LogicalPrimitive(!p.value, p.isNull); }

LogicalPrimitive operator&(const LogicalPrimitive &l, const LogicalPrimitive &r)
{
    if (!l.isNull && !l.value)
    { return LogicalPrimitive(false, false); }
    else
    { return LogicalPrimitive(l.value && r.value, l.isNull || r.isNull); }
}

LogicalPrimitive operator|(const LogicalPrimitive &l, const LogicalPrimitive &r)
{
    if (!l.isNull && l.value)
    { return LogicalPrimitive(true, false); }
    else
    { return LogicalPrimitive(l.value && r.value, l.isNull || r.isNull); }
}
// endregion

// region Copy constructors

template<> NullPrimitive::NullPrimitive(NullPrimitive t)
{}

template<> NumericPrimitive::NumericPrimitive(NumericPrimitive t)
        : value(t.value), isNull(t.isNull)
{}

template<> CharacterPrimitive::CharacterPrimitive(CharacterPrimitive t)
        : value(t.value), isNull(t.isNull)
{}

template<> LogicalPrimitive::LogicalPrimitive(LogicalPrimitive t)
        : value(t.value), isNull(t.isNull)
{}

template<> FunctionPrimitive::FunctionPrimitive(FunctionPrimitive t)
        : value(t.value), isNull(t.isNull)
{}

// endregion

// region Convert Null to anything

template<> NumericPrimitive::NumericPrimitive(NullPrimitive t)
        : value(), isNull(true)
{}

template<> CharacterPrimitive::CharacterPrimitive(NullPrimitive t)
        : value(), isNull(true)
{}

template<> LogicalPrimitive::LogicalPrimitive(NullPrimitive t)
        : value(), isNull(true)
{}

template<> FunctionPrimitive::FunctionPrimitive(NullPrimitive t)
        : value(), isNull(true)
{}

// endregion

// region Convert anything to Character

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"

template<> CharacterPrimitive::CharacterPrimitive(NumericPrimitive t)
        : value(std::trunc(t.value) == t.value ?
                std::to_string(static_cast<long long>(t.value)) : std::to_string(t.value)),
          isNull(t.isNull)
{}

#pragma GCC diagnostic pop

template<> CharacterPrimitive::CharacterPrimitive(LogicalPrimitive t)
        : value(t.value ? "TRUE" : "FALSE"), isNull(t.isNull)
{}

template<> CharacterPrimitive::CharacterPrimitive(FunctionPrimitive t)
        : value("function"), isNull(t.isNull)
{}
// endregion

// region Convert Logical to Numeric, Numeric to Logical and Numeric to index

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"

template<> LogicalPrimitive::LogicalPrimitive(NumericPrimitive t)
        : value(t.value != 0), isNull(t.isNull)
{}

#pragma GCC diagnostic pop

template<> NumericPrimitive::NumericPrimitive(LogicalPrimitive t)
        : value(t.value), isNull(t.isNull)
{}

NumericPrimitive::operator std::size_t()
{
    double floor = std::floor(value);
    return isNull || floor <= 0 ? 0 : std::size_t(floor);
}
// endregion