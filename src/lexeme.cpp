#include <string>

#include "lexeme.hpp"

Lexeme::Lexeme(LexemeType type, const std::string_view &contents)
        : lexemeType(type), lexemeContents(contents)
{}

Lexeme::LexemeType Lexeme::getType() const
{ return lexemeType; }

std::string Lexeme::getContents() const
{ return std::string(lexemeContents.begin(), lexemeContents.end()); }
