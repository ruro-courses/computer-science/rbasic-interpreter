#include "user_interface.hpp"
#include "lexer.hpp"
#include "parser.hpp"
#include "builtins.hpp"

int main(int argc, char *argv[])
{
    try
    {
        std::function<std::string(bool)> getter = argc == 2 ? getGetterFromFile(argv[1]) : getLineFromTerminal;
        Lexer lexer(getter);
        Parser parser(lexer);
        std::map<std::string, IVector> builtins{
                {"c", createBuiltin(concatenateBuiltin)},
                {"mode", createBuiltin(modeBuiltin)},
                {"length", createBuiltin(lengthBuiltin)}
        };
        ScopeStack ss(builtins);
        while (true)
        {
            IExpression exp = parser();
            IVector res = exp->execute(ss);
            if (typeid(*exp) != typeid(_AssignmentExpression))
            { std::cout << *res << std::endl; }
        }
    }
    catch (std::exception &re)
    { std::cerr << re.what() << std::endl; }
};