#include "ast.hpp"

_ConstantExpression::_ConstantExpression(IVector con)
        : value(std::move(con))
{}

_LogicalExpression::_LogicalExpression(std::vector<std::vector<std::pair<IExpression, bool>>> init)
        : or_exp(std::move(init))
{}

_IdentifierExpression::_IdentifierExpression(std::string ident)
        : identifier(std::move(ident))
{}

_AssignmentExpression::_AssignmentExpression(std::vector<IExpression> tar, IExpression src)
        : source(std::move(src)), targets(std::move(tar))
{}

_ComparativeExpression::_ComparativeExpression(IExpression lhs,
                                               _ComparativeExpression::ComparatorType ct,
                                               IExpression rhs)
        : type(ct), leftExp(std::move(lhs)), rightExp(std::move(rhs))
{}

_BlockExpression::_BlockExpression(std::vector<IExpression> init)
        : block(std::move(init))
{}

_CallExpression::_CallExpression(IExpression fun,
                                 std::vector<IExpression> pos,
                                 std::vector<std::pair<std::string, IExpression>> named)
        : function(std::move(fun)), posArgs(std::move(pos)), namedArgs(std::move(named))
{}

_IfExpression::_IfExpression(IExpression cond, IExpression onT, IExpression onF)
        : condition(std::move(cond)), onTrue(std::move(onT)), onFalse(std::move(onF))
{}

_WhileExpression::_WhileExpression(IExpression cond, IExpression bod)
        : condition(std::move(cond)), body(std::move(bod))
{}

_IndexedExpression::_IndexedExpression(IExpression dat, IExpression ind)
        : base(std::move(dat)), indices(std::move(ind))
{}

_RangeExpression::_RangeExpression(std::vector<IExpression> ran)
        : ranges(std::move(ran))
{}

_ProductExpression::_ProductExpression(std::vector<std::pair<IExpression, bool>> prod)
        : product(std::move(prod))
{}

_SumExpression::_SumExpression(std::vector<std::pair<IExpression, bool>> s)
        : sum(std::move(s))
{}

_FunctionExpression::_FunctionExpression(IExpression fun, std::vector<std::pair<std::string, IExpression>> args)
        : body(std::move(fun)), argsData(std::move(args))
{}

std::string _IExpression::getIdentifier()
{ except(fmt("Attempting to assign to a non-identifier expression")); }

std::string _IdentifierExpression::getIdentifier()
{ return identifier; }

std::string _IndexedExpression::getIdentifier()
{ return base->getIdentifier(); }

IVector _IndexedExpression::execute(ScopeStack &ss)
{
    IVector valV = base->execute(ss);
    IVector indV = indices->execute(ss);
    if (indV->getType() == PrimitiveType::LOGICAL_PRIMITIVE)
    {
        std::vector<LogicalPrimitive> mask = LogicalVector(*indV)->expand(valV->getLength())->getValue();
        std::vector<std::size_t> ind;
        for (std::size_t index = 0; index < mask.size(); ++index)
        {
            if (!mask[index].isNull && mask[index].value)
            { ind.push_back(index + 1); }
        }
        return valV->index(valV, ind);
    }

    std::vector<NumericPrimitive> np = NumericVector(*indV)->getValue();
    std::vector<std::size_t> ind(np.begin(), np.end());
    return valV->index(valV, ind);
}

IVector _AssignmentExpression::execute(ScopeStack &ss)
{
    IVector val = source->execute(ss);
    for (auto tar = targets.rbegin(); tar != targets.rend(); ++tar)
    {
        IExpression lhs = *tar;
        if (typeid(*lhs) == typeid(_IdentifierExpression))
        { ss.save(lhs->getIdentifier(), val); }
        else
        { ss.save(lhs->getIdentifier(), *lhs->execute(ss) <<= *val); }
    }
    return val;
}

IVector _ComparativeExpression::execute(ScopeStack &ss)
{
    switch (type)
    {
        case ComparatorType::EQ: return *leftExp->execute(ss) == *rightExp->execute(ss);
        case ComparatorType::NEQ: return *leftExp->execute(ss) != *rightExp->execute(ss);
        case ComparatorType::LT: return *leftExp->execute(ss) < *rightExp->execute(ss);
        case ComparatorType::GT: return *leftExp->execute(ss) > *rightExp->execute(ss);
        case ComparatorType::LTE: return *leftExp->execute(ss) <= *rightExp->execute(ss);
        case ComparatorType::GTE: return *leftExp->execute(ss) >= *rightExp->execute(ss);
        default: except(fmt("Invalid comparator type"));
    }
}

IVector _IdentifierExpression::execute(ScopeStack &ss)
{ return ss.load(identifier); }

IVector _LogicalExpression::execute(ScopeStack &ss)
{
    auto op_not =
            [&](std::pair<IExpression, bool> not_exp)
            { return not_exp.second ? !*not_exp.first->execute(ss) : not_exp.first->execute(ss); };
    auto multi_and =
            [&](std::vector<std::pair<IExpression, bool>> &and_exp)
            {
                auto and_it = and_exp.begin();
                if (and_it == and_exp.end())
                { except(fmt("Logical expression empty (and)")); }
                IVector and_v = op_not(*and_it);
                ++and_it;
                while (and_it != and_exp.end())
                { and_v = *and_v & *op_not(*and_it++); }
                return and_v;
            };
    auto or_it = or_exp.begin();
    if (or_it == or_exp.end())
    { except(fmt("Logical expression empty (or)")); }
    IVector or_v = multi_and(*or_it);
    ++or_it;
    while (or_it != or_exp.end())
    { or_v = *or_v | *multi_and(*or_it++); }
    return or_v;
}

IVector _ProductExpression::execute(ScopeStack &ss)
{
    auto p = product.begin();
    IVector iv = p->first->execute(ss);
    p++;
    for (; p != product.end(); ++p)
    {
        auto[exp, inv] = *p;
        iv = inv ? *iv / *exp->execute(ss) : *iv * *exp->execute(ss);
    }
    return iv;
}

IVector _SumExpression::execute(ScopeStack &ss)
{
    auto s = sum.begin();
    IVector iv = s->second ? -*s->first->execute(ss) : s->first->execute(ss);
    s++;
    for (; s != sum.end(); ++s)
    {
        auto[exp, min] = *s;
        iv = min ? *iv - *exp->execute(ss) : *iv + *exp->execute(ss);
    }
    return iv;
}

IVector _RangeExpression::execute(ScopeStack &ss)
{
    auto getRange =
            [](double start, double end)
            {
                std::vector<double> range;
                double sgn = std::copysign(1, end - start);
                uintmax_t count = 0;
                while (count <= sgn * (end - start))
                {
                    range.emplace_back(start + sgn * count);
                    count++;
                }
                return std::vector<NumericPrimitive>(range.begin(), range.end());
            };

    auto r = ranges.begin();
    IVector start = (*r)->execute(ss);
    NumericPrimitive start_ind = NumericVector(*start)->at(0);
    if (start_ind.isNull)
    { except(fmt("Null index")); }
    NumericPrimitive end_ind;
    IVector end;
    for (; r != ranges.end(); ++r)
    {
        end = (*r)->execute(ss);
        end_ind = NumericVector(*end)->at(0);
        if (end_ind.isNull)
        { except(fmt("Null index")); }
    }
    return createVector<DataVector<NumericVector>>(getRange(start_ind.value, end_ind.value));
}

IVector _BlockExpression::execute(ScopeStack &ss)
{
    IVector val;
    for (IExpression const &exp : block)
    { val = exp->execute(ss); }
    return val;
}

IVector _CallExpression::execute(ScopeStack &ss)
{
    FunctionPrimitive fun = FunctionVector(*function->execute(ss))->at(0);
    if (fun.isNull)
    { except(fmt("Attempting to call NULL")); }
    std::vector<IVector> pos;
    std::vector<std::pair<std::string, IVector>> named;
    for (IExpression const &posExp : posArgs)
    { pos.emplace_back(posExp->execute(ss)); }
    for (std::pair<std::string, IExpression> const &namedExp : namedArgs)
    { named.emplace_back(std::make_pair(namedExp.first, namedExp.second->execute(ss))); }
    return fun.value->call(pos, named, ss);
}

IVector _IfExpression::execute(ScopeStack &ss)
{
    LogicalPrimitive lp = LogicalVector(*condition->execute(ss))->at(0);
    if (!lp.isNull && lp.value)
    { return onTrue->execute(ss); }
    else
    { return onFalse->execute(ss); }
}

IVector _WhileExpression::execute(ScopeStack &ss)
{
    LogicalPrimitive lp = LogicalVector(*condition->execute(ss))->at(0);
    bool breaker = false;
    while (!lp.isNull && lp.value && !breaker)
    {
        try
        { body->execute(ss); }
        catch(_BreakExpression::Breaker &)
        { breaker = true; }
        if (!breaker)
        { lp = LogicalVector(*condition->execute(ss))->at(0); }
    }
    return createVector<DataVector<NullVector>>(std::vector<NullPrimitive>());
}

IVector _ConstantExpression::execute(ScopeStack &)
{ return value; }

IVector _NullExpression::execute(ScopeStack &)
{ return createVector<DataVector<NullVector>>(std::vector<NullPrimitive>()); }

IVector _BreakExpression::execute(ScopeStack &)
{ throw _BreakExpression::Breaker(); }

IVector _FunctionExpression::execute(ScopeStack &ss)
{
    std::vector<std::pair<std::string, IVector>> argsDefault;
    for (const std::pair<std::string, IExpression> &arg : argsData)
    { argsDefault.emplace_back(std::make_pair(arg.first, arg.second->execute(ss))); }
    return createVector<DataVector<FunctionVector>>(
            FunctionPrimitive(createCallable<NativeCallable>(body, argsDefault))
    );
}

_BreakExpression::Breaker::Breaker()
        : std::runtime_error("Break expression encountered outside of a while loop")
{}
