#include "callable.hpp"


IVector createBuiltin(std::function<IVector(std::vector<IVector>,
                                            std::vector<std::pair<std::string, IVector>>)> impl)
{
    ICallable builtin = createCallable<BuiltinCallable>(impl);
    FunctionPrimitive primitive(builtin);
    return createVector<DataVector<FunctionVector>>(primitive);
}

IVector _BuiltinCallable::call(std::vector<IVector> posArgs,
                               std::vector<std::pair<std::string, IVector>> namedArgs,
                               ScopeStack &ss)
{ return builtin(posArgs, namedArgs); }

_BuiltinCallable::_BuiltinCallable(std::function<IVector(std::vector<IVector>,
                                                         std::vector<std::pair<std::string, IVector>>)> impl)
        : builtin(std::move(impl))
{}

_NativeCallable::_NativeCallable(IExpression func, std::vector<std::pair<std::string, IVector>> args)
        : function(std::move(func)), argList(std::move(args))
{}

IVector _NativeCallable::call(std::vector<IVector> posArgs,
                              std::vector<std::pair<std::string, IVector>> namedArgs,
                              ScopeStack &ss)
{
    ss.pushScope();
    if (posArgs.size() > argList.size())
    { except(fmt("Expected up to ", argList.size(), " positional arguments, got ", posArgs.size())); }
    for (std::size_t index = 0; index < argList.size(); ++index)
    {
        if (index < posArgs.size())
        { ss.save(argList[index].first, posArgs[index]); }
        else
        { ss.save(argList[index].first, argList[index].second); }
    }
    for (std::pair<std::string, IVector> &namedArg : namedArgs)
    { ss.save(namedArg.first, namedArg.second); }
    IVector res = function->execute(ss);
    ss.popScope();
    return res;
}
