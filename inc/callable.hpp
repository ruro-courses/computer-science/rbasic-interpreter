#ifndef PROJECT_RB_CALLABLE_HPP
#define PROJECT_RB_CALLABLE_HPP

// region Std Include
#include <memory>
#include <vector>
// endregion

// region Forward-Declaration
template<class T>
using SP = std::shared_ptr<T>;

class _ICallable;
class _BuiltinCallable;
class _NativeCallable;

using ICallable = SP<_ICallable>;
using BuiltinCallable = SP<_BuiltinCallable>;
using NativeCallable = SP<_NativeCallable>;

template<class T, class...Args>
T createCallable(Args &&...args)
{ return T(new class T::element_type(std::forward<Args>(args)...)); }
// endregion

// region Internal Include
#include "vector.hpp"
#include "scope.hpp"
// endregion

// region Declaration

class _ICallable
{
public:
    virtual ~_ICallable() = default;
    virtual IVector call(std::vector<IVector> posArgs,
                         std::vector<std::pair<std::string, IVector>> namedArgs,
                         ScopeStack &) = 0;
protected:
    _ICallable() = default;
};

class _BuiltinCallable final : public _ICallable
{
    template<class T, class...Args> friend T createCallable(Args &&...args);
public:
    IVector call(std::vector<IVector> posArgs,
                 std::vector<std::pair<std::string, IVector>> namedArgs,
                 ScopeStack &ss) final;
protected:
    std::function<IVector(std::vector<IVector>, std::vector<std::pair<std::string, IVector>>)> builtin;
    explicit _BuiltinCallable(std::function<IVector(std::vector<IVector>,
                                                    std::vector<std::pair<std::string, IVector>>)> impl);
};

class _NativeCallable final : public _ICallable
{
    template<class T, class...Args> friend T createCallable(Args &&...args);
public:
    IVector call(std::vector<IVector> posArgs,
                 std::vector<std::pair<std::string, IVector>> namedArgs,
                 ScopeStack &ss) final;
protected:
    IExpression function;
    std::vector<std::pair<std::string, IVector>> argList;
    explicit _NativeCallable(IExpression func, std::vector<std::pair<std::string, IVector>> args);
};
// endregion

#endif //PROJECT_RB_CALLABLE_HPP
