#ifndef PROJECT_RB_AST_HPP
#define PROJECT_RB_AST_HPP

// region Std Include
#include <memory>
#include <vector>
#include <cmath>
// endregion

// region Forward-Declaration
template<class T>
using SP = std::shared_ptr<T>;

class _IExpression;
class _ConstantExpression;
class _NullExpression;
class _LogicalExpression;
class _IdentifierExpression;
class _AssignmentExpression;
class _ProductExpression;
class _SumExpression;
class _ComparativeExpression;
class _RangeExpression;
class _BlockExpression;
class _IfExpression;
class _WhileExpression;
class _BreakExpression;
class _IndexedExpression;
class _CallExpression;
class _FunctionExpression;

using IExpression = SP<_IExpression>;
using ConstantExpression = SP<_ConstantExpression>;
using NullExpression = SP<_NullExpression>;
using LogicalExpression = SP<_LogicalExpression>;
using IdentifierExpression = SP<_IdentifierExpression>;
using AssignmentExpression = SP<_AssignmentExpression>;
using ProductExpression = SP<_ProductExpression>;
using SumExpression = SP<_SumExpression>;
using ComparativeExpression = SP<_ComparativeExpression>;
using RangeExpression = SP<_RangeExpression>;
using BlockExpression = SP<_BlockExpression>;
using IfExpression = SP<_IfExpression>;
using WhileExpression = SP<_WhileExpression>;
using BreakExpression = SP<_BreakExpression>;
using IndexedExpression = SP<_IndexedExpression>;
using CallExpression = SP<_CallExpression>;
using FunctionExpression = SP<_FunctionExpression>;

template<class T, class...Args>
T createExpression(Args &&...args)
{ return T(new class T::element_type(std::forward<Args>(args)...)); }
// endregion

// region Internal Include
#include "primitive.hpp"
#include "vector.hpp"
#include "scope.hpp"
// endregion

// region Declaration

class _IExpression
{
public:
    virtual ~_IExpression() = default;
    virtual IVector execute(ScopeStack &) = 0;
    virtual std::string getIdentifier();

protected:
    _IExpression() = default;
};

class _ConstantExpression final : public _IExpression
{
    template<class T, class...Args> friend T createExpression(Args &&...args);
public:
    IVector execute(ScopeStack &) override;
protected:
    IVector value;
    explicit _ConstantExpression(IVector con);
};

class _NullExpression final : public _IExpression
{
    template<class T, class...Args> friend T createExpression(Args &&...args);
public:
    IVector execute(ScopeStack &) override;
protected:
    _NullExpression() = default;
};

class _LogicalExpression final : public _IExpression
{
    template<class T, class...Args> friend T createExpression(Args &&...args);
public:
    IVector execute(ScopeStack &) override;
protected:
    std::vector<std::vector<std::pair<IExpression, bool>>> or_exp;
    explicit _LogicalExpression(std::vector<std::vector<std::pair<IExpression, bool>>> init);
};

class _IdentifierExpression final : public _IExpression
{
    template<class T, class...Args> friend T createExpression(Args &&...args);
public:
    IVector execute(ScopeStack &) override;
    std::string getIdentifier() override;
protected:
    std::string identifier;
    explicit _IdentifierExpression(std::string ident);
};

class _AssignmentExpression final : public _IExpression
{
    template<class T, class...Args> friend T createExpression(Args &&...args);
public:
    IVector execute(ScopeStack &) override;
protected:
    IExpression source;
    std::vector<IExpression> targets;
    explicit _AssignmentExpression(std::vector<IExpression> tar, IExpression src);
};

class _ProductExpression final : public _IExpression
{
    template<class T, class...Args> friend T createExpression(Args &&...args);
public:
    IVector execute(ScopeStack &) override;
protected:
    std::vector<std::pair<IExpression, bool>> product;
    explicit _ProductExpression(std::vector<std::pair<IExpression, bool>> prod);
};

class _SumExpression final : public _IExpression
{
    template<class T, class...Args> friend T createExpression(Args &&...args);
public:
    IVector execute(ScopeStack &) override;
protected:
    std::vector<std::pair<IExpression, bool>> sum;
    explicit _SumExpression(std::vector<std::pair<IExpression, bool>> s);
};

class _ComparativeExpression final : public _IExpression
{
    template<class T, class...Args> friend T createExpression(Args &&...args);
public:
    enum class ComparatorType
    {
        EQ, NEQ, LT, GT, LTE, GTE
    };
    IVector execute(ScopeStack &) override;
protected:
    ComparatorType type;
    IExpression leftExp;
    IExpression rightExp;
    explicit _ComparativeExpression(IExpression lhs, ComparatorType ct, IExpression rhs);
};

class _RangeExpression final : public _IExpression
{
    template<class T, class...Args> friend T createExpression(Args &&...args);
public:
    IVector execute(ScopeStack &) override;
protected:
    std::vector<IExpression> ranges;
    explicit _RangeExpression(std::vector<IExpression> ran);
};

class _BlockExpression final : public _IExpression
{
    template<class T, class...Args> friend T createExpression(Args &&...args);
public:
    IVector execute(ScopeStack &) override;
protected:
    std::vector<IExpression> block;
    explicit _BlockExpression(std::vector<IExpression> init);
};

class _IfExpression final : public _IExpression
{
    template<class T, class...Args> friend T createExpression(Args &&...args);
public:
    IVector execute(ScopeStack &) override;
protected:
    IExpression condition;
    IExpression onTrue;
    IExpression onFalse;
    explicit _IfExpression(IExpression cond, IExpression onT, IExpression onF);
};

class _WhileExpression final : public _IExpression
{
    template<class T, class...Args> friend T createExpression(Args &&...args);
public:
    IVector execute(ScopeStack &) override;
protected:
    IExpression condition;
    IExpression body;
    explicit _WhileExpression(IExpression cond, IExpression bod);
};

class _BreakExpression final : public _IExpression
{
    template<class T, class...Args> friend T createExpression(Args &&...args);
public:
    class Breaker final : public std::runtime_error
    {
    public:
        Breaker();
    };
    IVector execute(ScopeStack &) override;
protected:
    explicit _BreakExpression() = default;
};

class _IndexedExpression final : public _IExpression
{
    template<class T, class...Args> friend T createExpression(Args &&...args);
public:
    IVector execute(ScopeStack &) override;
    std::string getIdentifier() override;
protected:
    IExpression base;
    IExpression indices;
    explicit _IndexedExpression(IExpression dat, IExpression ind);
};

class _CallExpression final : public _IExpression
{
    template<class T, class...Args> friend T createExpression(Args &&...args);
public:
    IVector execute(ScopeStack &) override;
protected:
    IExpression function;
    std::vector<IExpression> posArgs;
    std::vector<std::pair<std::string, IExpression>> namedArgs;
    explicit _CallExpression(IExpression fun, std::vector<IExpression> pos,
                             std::vector<std::pair<std::string, IExpression>> named);
};

class _FunctionExpression final : public _IExpression
{
    template<class T, class...Args> friend T createExpression(Args &&...args);
public:
    IVector execute(ScopeStack &) override;
protected:
    IExpression body;
    std::vector<std::pair<std::string, IExpression>> argsData;
    explicit _FunctionExpression(IExpression fun, std::vector<std::pair<std::string, IExpression>> args);
};

// endregion

#endif //PROJECT_RB_AST_HPP