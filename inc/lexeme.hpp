#ifndef PROJECT_RB_LEXEM_HPP
#define PROJECT_RB_LEXEM_HPP

// region Std Include
#include <string_view>
// endregion

// region Forward-Declaration

class Lexeme;
// endregion

// region Internal Include
#include "user_interface.hpp"
// endregion

// region Declaration

class Lexeme
{
public:
    enum class LexemeType
    {
        LEX_SEMICOLON,          ///< `;`
        LEX_LINE,               ///< `\n`
        LEX_IDENTIFIER,         ///< `[_AZaz][._AZaz09]*` or `.[._AZaz][._AZaz09]*`
        LEX_NUMERIC,            ///< `[09]*(.)[09]+`
        LEX_CHARACTER,          ///< `\"[^\"]*\"`
        LEX_CONSTANT,           ///< `NULL` or `FALSE` or `TRUE`, also reserved for future constants
        LEX_KEYWORD,            ///< `function` or `while`, also reserved for future keywords
        LEX_PAREN,              ///< `(` or `)`
        LEX_INDEX,              ///< `[` or `]`
        LEX_BLOCK,              ///< `{` or `}`
        LEX_ARGUMENT_ASSIGN,    ///< `a=b`  in function argument lists
        LEX_ARGUMENT_COMMA,     ///< `a,b` in function argument lists
        LEX_OP_ASSIGN,          ///< `a<-b`
        LEX_OP_SUM,             ///< `a+b` `a-b` `+a` `-a`
        LEX_OP_PRODUCT,         ///< `a*b` `a/b`
        LEX_OP_RANGE,           ///< `a:b`
        LEX_OP_LOGIC,           ///< `a&b` `a|b` `!a`
        LEX_OP_COMPARE,         ///< `a<b` `a>b` `a==b` `a!=b` `a<=b` `a>=b`
    };
private:
    LexemeType lexemeType;
    std::string_view lexemeContents;
public:
    explicit Lexeme(LexemeType type, const std::string_view &contents);
    LexemeType getType() const;
    std::string getContents() const;
};

constexpr const char *operator*(Lexeme::LexemeType vt)
{
    using LexemeType = Lexeme::LexemeType;
    switch (vt)
    {
        case LexemeType::LEX_SEMICOLON: return "semicolon";
        case LexemeType::LEX_LINE: return "new line";
        case LexemeType::LEX_IDENTIFIER: return "identifier";
        case LexemeType::LEX_NUMERIC: return "numeric literal";
        case LexemeType::LEX_CHARACTER: return "character literal";
        case LexemeType::LEX_CONSTANT: return "constant";
        case LexemeType::LEX_KEYWORD: return "keyword";
        case LexemeType::LEX_PAREN: return "parenthesis";
        case LexemeType::LEX_INDEX: return "index";
        case LexemeType::LEX_BLOCK: return "block";
        case LexemeType::LEX_ARGUMENT_ASSIGN: return "argument assignment";
        case LexemeType::LEX_ARGUMENT_COMMA: return "argument comma";
        case LexemeType::LEX_OP_ASSIGN: return "assignment operator";
        case LexemeType::LEX_OP_SUM: return "sum operator";
        case LexemeType::LEX_OP_PRODUCT: return "product operator";
        case LexemeType::LEX_OP_RANGE: return "range operator";
        case LexemeType::LEX_OP_LOGIC: return "logic operator";
        case LexemeType::LEX_OP_COMPARE: return "compare operator";
        default: except("Unknown Primitive type exception");
    }
}
// endregion

#endif //PROJECT_RB_LEXEM_HPP
