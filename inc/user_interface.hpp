#ifndef PROJECT_RB_USER_INTERFACE_HPP
#define PROJECT_RB_USER_INTERFACE_HPP

// region Std Include
#include <iostream>
#include <sstream>
#include <functional>
#include <fstream>
// endregion

// region Declaration

std::string getLineFromTerminal(bool continuation);
std::function<std::string(bool)> getGetterFromFile(std::string filename);
[[noreturn]] void except(const std::string &message);

template<class...Args>
std::string fmt(Args...args)
{
    std::ostringstream oss;
    // Expand args pack, applying oss << arg for each arg.
    (void) (int[]){0, (void(oss << args), 0)...};
    return oss.str();
}
// endregion

#endif //PROJECT_RB_USER_INTERFACE_HPP
