#ifndef PROJECT_RB_LEXER_HPP
#define PROJECT_RB_LEXER_HPP

// region Std Include
#include <vector>
#include <functional>
#include <string_view>
// endregion

// region Forward-Declaration

class Lexer;
// endregion

// region Internal Include
#include "user_interface.hpp"
#include "lexeme.hpp"
// endregion

// region Declaration

class Lexer
{
private:
    std::function<std::string(bool)> source;
    std::vector<std::string> lines;
    std::vector<std::string>::size_type currentLineNum;
    std::string::size_type currentSymbolNum;
    
    Lexeme extractCharacter();
    Lexeme extractNumeric();
    Lexeme extractIdentifier();
    Lexeme extractComment();
    Lexeme extractLexeme(Lexeme::LexemeType lt, std::string::size_type len);
    Lexeme nextLexem();
    std::string &currentLine(std::string::size_type offset = 0);
    std::string::size_type isPrefixedWith(const std::string &prefix);
    char currentSymbol(std::string::size_type offset = 0);
    void skipWhiteSpace();
    void readMore(bool continuation);
public:
    explicit Lexer(std::function<std::string(bool)> src);
    Lexeme operator()(bool continuation);
};
// endregion

#endif //PROJECT_RB_LEXER_HPP