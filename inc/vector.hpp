#ifndef PROJECT_RB_VECTOR_HPP
#define PROJECT_RB_VECTOR_HPP

// region Std Include
#include <memory>
#include <vector>
#include <algorithm>
// endregion

// region Forward-Declaration
template<class T>
using SP = std::shared_ptr<T>;

class _IVector;
using IVector = SP<_IVector>;

enum class PrimitiveType;

template<class primitiveType, PrimitiveType enumType>
class _TypedVector;
template<class primitiveType, PrimitiveType enumType>
using TypedVector = SP<_TypedVector<primitiveType, enumType>>;

template<class baseType>
class _DataVector;
template<class baseType>
using DataVector = SP<_DataVector<class baseType::element_type>>;

template<class baseType>
class _IndexedVector;
template<class baseType>
using IndexedVector = SP<_IndexedVector<class baseType::element_type>>;

template<class T, class...Args>
T createVector(Args &&...args)
{ return T(new class T::element_type(std::forward<Args>(args)...)); }
// endregion

// region Internal Include
#include "user_interface.hpp"
#include "primitive.hpp"
// endregion

// region Declaration

// region Aliasing
using _NullVector = _TypedVector<NullPrimitive, PrimitiveType::NULL_PRIMITIVE>;
using _NumericVector = _TypedVector<NumericPrimitive, PrimitiveType::NUMERIC_PRIMITIVE>;
using _CharacterVector = _TypedVector<CharacterPrimitive, PrimitiveType::CHARACTER_PRIMITIVE>;
using _LogicalVector = _TypedVector<LogicalPrimitive, PrimitiveType::LOGICAL_PRIMITIVE>;
using _FunctionVector = _TypedVector<FunctionPrimitive, PrimitiveType::FUNCTION_PRIMITIVE>;

using NullVector = SP<_NullVector>;
using NumericVector = SP<_NumericVector>;
using CharacterVector = SP<_CharacterVector>;
using LogicalVector = SP<_LogicalVector>;
using FunctionVector = SP<_FunctionVector>;
// endregion

// region Generics

class _IVector
{
public:
    virtual PrimitiveType getType() const = 0;
    virtual std::size_t getLength() const = 0;
    virtual IVector index(IVector self, std::vector<std::size_t> indices) const = 0;

    explicit virtual operator NullVector() const = 0;
    explicit virtual operator NumericVector() const = 0;
    explicit virtual operator CharacterVector() const = 0;
    explicit virtual operator LogicalVector() const = 0;
    explicit virtual operator FunctionVector() const = 0;

    virtual ~_IVector() = default;
protected:
    explicit _IVector() = default;
};

template<class primitiveType, PrimitiveType enumType>
class _TypedVector : public _IVector
{
public:
    using pType = primitiveType;
    using vType = TypedVector<primitiveType, enumType>;
    static const PrimitiveType eType = enumType;
    PrimitiveType getType() const final;
    IVector index(IVector self, std::vector<std::size_t> indices) const final;
    TypedVector<pType, eType> assign(const _TypedVector<pType, eType> &rhs) const;

    virtual TypedVector<pType, eType> expand(std::size_t sz) const = 0;
    virtual std::vector<pType> getValue() const = 0;
    virtual std::vector<pType> getData() const = 0;
    virtual bool isAssignable() const = 0;
    virtual std::vector<std::size_t> unindex() const = 0;
    virtual pType at(std::size_t pos) const = 0;
protected:
    explicit _TypedVector() = default;
};

template<class baseType>
class _DataVector final : public baseType
{
    template<class T, class...Args>
    friend T createVector(Args &&...args);
public:
    explicit operator NullVector() const final;
    explicit operator NumericVector() const final;
    explicit operator CharacterVector() const final;
    explicit operator LogicalVector() const final;
    explicit operator FunctionVector() const final;

    std::size_t getLength() const final;
    TypedVector<class baseType::pType, baseType::eType> expand(std::size_t sz) const final;
    std::vector<class baseType::pType> getValue() const final;
    std::vector<class baseType::pType> getData() const final;
    bool isAssignable() const final;
    std::vector<std::size_t> unindex() const final;
    class baseType::pType at(std::size_t pos) const final;
protected:
    std::vector<class baseType::pType> data;
    bool assignable;

    template<class primitiveType>
    explicit _DataVector(const std::vector<primitiveType> &init, bool assignable = true);

    template<class primitiveType>
    explicit _DataVector(primitiveType init);
};

template<class baseType>
class _IndexedVector final : public baseType
{
    template<class T, class...Args>
    friend T createVector(Args &&...args);
public:
    explicit operator NullVector() const final;
    explicit operator NumericVector() const final;
    explicit operator CharacterVector() const final;
    explicit operator LogicalVector() const final;
    explicit operator FunctionVector() const final;

    std::size_t getLength() const final;
    TypedVector<class baseType::pType, baseType::eType> expand(std::size_t sz) const final;
    std::vector<class baseType::pType> getValue() const final;
    std::vector<class baseType::pType> getData() const final;
    bool isAssignable() const final;
    class baseType::pType at(std::size_t pos) const final;
    std::vector<std::size_t> unindex() const final;
protected:
    class baseType::vType source;
    std::vector<std::size_t> indices;
    explicit _IndexedVector() = default;
    explicit _IndexedVector(IVector src, std::vector<std::size_t> ind);
};
// endregion

// region Vector operators

std::ostream &operator<<(std::ostream &os, const _IVector &v);
NumericVector operator+(const _IVector &l, const _IVector &r);
NumericVector operator-(const _IVector &l, const _IVector &r);
NumericVector operator*(const _IVector &l, const _IVector &r);
NumericVector operator/(const _IVector &l, const _IVector &r);
LogicalVector operator&(const _IVector &l, const _IVector &r);
LogicalVector operator|(const _IVector &l, const _IVector &r);
LogicalVector operator<(const _IVector &l, const _IVector &r);
LogicalVector operator>(const _IVector &l, const _IVector &r);
LogicalVector operator<=(const _IVector &l, const _IVector &r);
LogicalVector operator>=(const _IVector &l, const _IVector &r);
LogicalVector operator==(const _IVector &l, const _IVector &r);
LogicalVector operator!=(const _IVector &l, const _IVector &r);

IVector operator<<=(const _IVector &l, const _IVector &r);
NumericVector operator+(const _IVector &v);
NumericVector operator-(const _IVector &v);
LogicalVector operator!(const _IVector &v);
// endregion
// endregion

#endif //PROJECT_RB_VECTOR_HPP
