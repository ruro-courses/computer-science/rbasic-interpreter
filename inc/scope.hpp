#ifndef PROJECT_RB_SCOPE_HPP
#define PROJECT_RB_SCOPE_HPP

// region Std Include
#include <stack>
#include <map>
// endregion

// region Forward-Declaration

class ScopeStack;
// endregion

// region Internal Include
#include "vector.hpp"
// endregion

// region Declaration

class ScopeStack
{
public:
    IVector load(const std::string &name);
    bool check(const std::string &name);
    void save(const std::string &name, IVector value);
    void pushScope();
    void popScope();
    explicit ScopeStack(std::map<std::string, IVector> builtins);
protected:
    std::vector<std::map<std::string, IVector>> ss;
};
// endregion

#endif //PROJECT_RB_SCOPE_HPP
