#ifndef PROJECT_RB_BUILTINS_HPP
#define PROJECT_RB_BUILTINS_HPP

#include <functional>
#include <vector>
#include <numeric>

#include "vector.hpp"
#include "callable.hpp"
#include "user_interface.hpp"

IVector concatenateBuiltin(std::vector<IVector> posArgs, std::vector<std::pair<std::string, IVector>> namedArgs);
IVector modeBuiltin(std::vector<IVector> posArgs, std::vector<std::pair<std::string, IVector>> namedArgs);
IVector lengthBuiltin(std::vector<IVector> posArgs, std::vector<std::pair<std::string, IVector>> namedArgs);
IVector createBuiltin(std::function < IVector(std::vector < IVector > ,
                                              std::vector < std::pair < std::string, IVector >> ) > impl);

#endif //PROJECT_RB_BUILTINS_HPP
