#ifndef PROJECT_RB_PRIMITIVE_HPP
#define PROJECT_RB_PRIMITIVE_HPP

// region Std Include
#include <exception>
#include <memory>
// endregion

//region Forward-Declaration

class NullPrimitive;
class NumericPrimitive;
class CharacterPrimitive;
class LogicalPrimitive;
class FunctionPrimitive;

enum class PrimitiveType
{
    NULL_PRIMITIVE,
    NUMERIC_PRIMITIVE,
    CHARACTER_PRIMITIVE,
    LOGICAL_PRIMITIVE,
    FUNCTION_PRIMITIVE,
};

constexpr const char *operator*(PrimitiveType vt);
//endregion

// region Internal Include
#include "user_interface.hpp"
#include "ast.hpp"
#include "callable.hpp"
// endregion

// region Declaration

class NullPrimitive
{
public:
    static const bool isNull = true;
    explicit NullPrimitive() = default;

    template<class T>
    explicit NullPrimitive(T)
    { except("Null primitive conversion exception"); }
};

class NumericPrimitive
{
public:
    double value;
    bool isNull;
    explicit NumericPrimitive();
    explicit NumericPrimitive(double v, bool n);

    template<class T>
    explicit NumericPrimitive(T) : value(), isNull()
    { except("Numeric Primitive exception"); }

    explicit operator std::size_t();
};

class CharacterPrimitive
{
public:
    std::string value;
    bool isNull;
    explicit CharacterPrimitive();
    explicit CharacterPrimitive(std::string s, bool n);

    template<class T>
    explicit CharacterPrimitive(T) : value(), isNull()
    { except("Character Primitive exception"); }
};

class LogicalPrimitive
{
public:
    bool value;
    bool isNull;
    explicit LogicalPrimitive();
    explicit LogicalPrimitive(bool v, bool n);

    template<class T>
    explicit LogicalPrimitive(T) : value(), isNull()
    { except("Logical Primitive exception"); }
};

class FunctionPrimitive
{
public:
    ICallable value;
    bool isNull;
    explicit FunctionPrimitive();
    explicit FunctionPrimitive(ICallable v, bool n);

    template<class T>
    explicit FunctionPrimitive(T) : value(), isNull()
    { except("Function Primitive exception"); }
};

template<> NumericPrimitive::NumericPrimitive(double);
template<> CharacterPrimitive::CharacterPrimitive(std::string);
template<> LogicalPrimitive::LogicalPrimitive(bool);
template<> FunctionPrimitive::FunctionPrimitive(ICallable);
template<> NullPrimitive::NullPrimitive(NullPrimitive);
template<> NumericPrimitive::NumericPrimitive(NumericPrimitive);
template<> CharacterPrimitive::CharacterPrimitive(CharacterPrimitive);
template<> LogicalPrimitive::LogicalPrimitive(LogicalPrimitive);
template<> FunctionPrimitive::FunctionPrimitive(FunctionPrimitive);
template<> NumericPrimitive::NumericPrimitive(NullPrimitive);
template<> CharacterPrimitive::CharacterPrimitive(NullPrimitive);
template<> LogicalPrimitive::LogicalPrimitive(NullPrimitive);
template<> FunctionPrimitive::FunctionPrimitive(NullPrimitive);
template<> CharacterPrimitive::CharacterPrimitive(NumericPrimitive);
template<> CharacterPrimitive::CharacterPrimitive(LogicalPrimitive);
template<> CharacterPrimitive::CharacterPrimitive(FunctionPrimitive);
template<> LogicalPrimitive::LogicalPrimitive(NumericPrimitive);
template<> NumericPrimitive::NumericPrimitive(LogicalPrimitive);

NumericPrimitive operator+(const NumericPrimitive &p);
NumericPrimitive operator-(const NumericPrimitive &p);
NumericPrimitive operator+(const NumericPrimitive &l, const NumericPrimitive &r);
NumericPrimitive operator-(const NumericPrimitive &l, const NumericPrimitive &r);
NumericPrimitive operator*(const NumericPrimitive &l, const NumericPrimitive &r);
NumericPrimitive operator/(const NumericPrimitive &l, const NumericPrimitive &r);
LogicalPrimitive operator<(const NumericPrimitive &l, const NumericPrimitive &r);
LogicalPrimitive operator>(const NumericPrimitive &l, const NumericPrimitive &r);
LogicalPrimitive operator<=(const NumericPrimitive &l, const NumericPrimitive &r);
LogicalPrimitive operator>=(const NumericPrimitive &l, const NumericPrimitive &r);
LogicalPrimitive operator==(const NumericPrimitive &l, const NumericPrimitive &r);
LogicalPrimitive operator!=(const NumericPrimitive &l, const NumericPrimitive &r);
LogicalPrimitive operator<(const CharacterPrimitive &l, const CharacterPrimitive &r);
LogicalPrimitive operator>(const CharacterPrimitive &l, const CharacterPrimitive &r);
LogicalPrimitive operator<=(const CharacterPrimitive &l, const CharacterPrimitive &r);
LogicalPrimitive operator>=(const CharacterPrimitive &l, const CharacterPrimitive &r);
LogicalPrimitive operator==(const CharacterPrimitive &l, const CharacterPrimitive &r);
LogicalPrimitive operator!=(const CharacterPrimitive &l, const CharacterPrimitive &r);
LogicalPrimitive operator!(const LogicalPrimitive &p);
LogicalPrimitive operator&(const LogicalPrimitive &l, const LogicalPrimitive &r);
LogicalPrimitive operator|(const LogicalPrimitive &l, const LogicalPrimitive &r);

constexpr const char *operator*(PrimitiveType vt)
{
    switch (vt)
    {
        case PrimitiveType::NULL_PRIMITIVE: return "NULL";
        case PrimitiveType::NUMERIC_PRIMITIVE: return "numeric";
        case PrimitiveType::CHARACTER_PRIMITIVE: return "character";
        case PrimitiveType::LOGICAL_PRIMITIVE: return "logical";
        case PrimitiveType::FUNCTION_PRIMITIVE: return "function";
        default: except("Unknown Primitive type exception");
    }
}
// endregion

#endif //PROJECT_RB_PRIMITIVE_HPP
