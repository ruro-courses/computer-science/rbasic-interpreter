#ifndef PROJECT_RB_PARSER_HPP
#define PROJECT_RB_PARSER_HPP

#include <vector>
#include <functional>
#include <string>

class Parser;

#include "lexeme.hpp"
#include "ast.hpp"
#include "vector.hpp"

class Parser
{
private:
    std::function<Lexeme(bool)> source;
    std::vector<Lexeme> lexems;
    std::vector<Lexeme>::size_type currentPosition;
    std::size_t expressionDepth;

    IExpression getNamed(); ///< Identifier, Constant or Literal
    IExpression getSpecial(); ///< while, if, break or function, otherwise getNamed
    IExpression getParenGroup(); ///< get contents of the expression in parenthesis
    IExpression getSubExpression();  ///< { ... } or ( ... ), otherwise getSpecial
    IExpression getElementary(); ///< getSubexpression and an optional sequence of [indexed] and (called)
    IExpression getRange(); ///< getElementary(){:getElementary()}
    IExpression getProduct(); ///< getRange(){[*/]getRange()}
    IExpression getSum(); ///< [+-]getProduct(){[+-]getProduct()}
    IExpression getComparative(); ///< getSum(){[<>==!=<=>=]getSum()}
    IExpression getLogical(); ///< {!}getComparative(){[|&] {!}getComparative()}
    IExpression getExpression(); ///< getLogical() {<- getLogical()}
    const Lexeme &at(std::vector<Lexeme>::size_type pos = 0, bool force_continuation = false);
public:
    explicit Parser(std::function<Lexeme(bool)> src);
    IExpression operator()();
};

#endif //PROJECT_RB_PARSER_HPP
