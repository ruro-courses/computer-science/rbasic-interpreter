CXX       = g++
CPPFLAGS += -O3 -I$(INCDIR) -Wall -Werror -Wformat-security -Wignored-qualifiers -Winit-self -Wswitch-default -Wfloat-equal -Wshadow -Wpointer-arith -Wtype-limits -Wempty-body -Wlogical-op -Wmissing-field-initializers -Wctor-dtor-privacy  -Wnon-virtual-dtor -Wstrict-null-sentinel  -Wold-style-cast -Woverloaded-virtual -Wsign-promo -Weffc++ -std=gnu++17
DEPFLAGS += -MT $@ -MMD -MP -MF $(DEPDIR)/$*.Td
LDFLAGS  += -O3 -Wall -Werror -Wformat-security -Wignored-qualifiers -Winit-self -Wswitch-default -Wfloat-equal -Wshadow -Wpointer-arith -Wtype-limits -Wempty-body -Wlogical-op -Wmissing-field-initializers -Wctor-dtor-privacy  -Wnon-virtual-dtor -Wstrict-null-sentinel  -Wold-style-cast -Woverloaded-virtual -Wsign-promo -Weffc++ -std=gnu++17

SRCDIR   := src
INCDIR   := inc
OBJDIR   := obj
DEPDIR   := dep
BINDIR   := bin
DOCDIR	 := docs

SRCFILES := $(wildcard $(SRCDIR)/*.cpp)
SRCNAMES := $(patsubst $(SRCDIR)/%.cpp, %, $(SRCFILES))
COMNAMES := $(filter-out $(BINDIR)_%, $(SRCNAMES))
BINNAMES := $(filter     $(BINDIR)_%, $(SRCNAMES))
OBJFILES := $(patsubst %, $(OBJDIR)/%.o, $(COMNAMES) $(BINNAMES))
DEPFILES := $(patsubst %, $(DEPDIR)/%.d, $(COMNAMES) $(BINNAMES))
COMFILES := $(patsubst %, $(OBJDIR)/%.o, $(COMNAMES))
BINFILES := $(patsubst $(BINDIR)_%, $(BINDIR)/$(BINDIR)_%, $(BINNAMES))

$(shell mkdir -p $(OBJDIR) >/dev/null)
$(shell mkdir -p $(DEPDIR) >/dev/null)
$(shell mkdir -p $(BINDIR) >/dev/null)
$(shell mkdir -p $(DOCDIR) >/dev/null)

.PHONY: _info all clean $(DOCDIR) $(BINDIR) $(OBJDIR)
_info:
	@echo -e "Known targets:"
	@echo -e "all\t-\tcompile all"
	@echo -e "$(DOCDIR)\t-\tcompile the documentation"
	@echo -e "clean\t-\tremove all makeable files"
	@echo -e "$(BINDIR)\t-\tcompile runnable binary files only"
	@echo -e "$(OBJDIR)\t-\tcompile object files only"
all: $(OBJDIR) $(BINDIR) $(DOCDIR)
$(OBJDIR): $(OBJFILES)
$(BINDIR): $(BINFILES)
clean:
	@$(RM) -rf $(OBJDIR) $(BINDIR) $(DEPDIR) $(DOCDIR)
$(DOCDIR):
	doxygen doxygen.conf/doxyfile

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
$(OBJDIR)/%.o: $(SRCDIR)/%.cpp $(DEPDIR)/%.d
	$(CXX) $(DEPFLAGS) $(CPPFLAGS) -c $< $(OUTPUT_OPTION)
	@mv -f $(DEPDIR)/$*.Td $(DEPDIR)/$*.d && touch $@

$(BINDIR)/%: $(OBJDIR)/%.o $(COMFILES)
	$(CXX) $(LDFLAGS) $^ $(OUTPUT_OPTION)

$(DEPDIR)/%.d: ;
.PRECIOUS: $(DEPDIR)/%.d

include $(DEPFILES)
